// Add fee backend behavior
Drupal.behaviors.ucCCfeeBackend = function(context) {
  // on admin/store/settings/payment/edit/methods
  $(context)
    .find('#uc-cc-fee-settings-table-form tbody tr')
      .each(function(i, row) {
        // Don't know why but .hide() method wont work..
        $(row).find('td:last').attr('style', 'display:none');
      });

  // on admin/store/orders/%uc_order/credit
  $(context)
    .find('#uc_cc_fee input:radio')
      .change(function() {
        var fee = Drupal.settings.ucCCfee[$(this).val()];
        $('#uc_cc_fee_label_value')
          .text(fee.formatted);
      });
};
