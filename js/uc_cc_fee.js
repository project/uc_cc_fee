
// Create own namespace
Drupal.ucCCfee = Drupal.ucCCfee || {};

// On page load run own fees behavior
Drupal.behaviors.ucCCfee = function(context) {

  // Remember basic elements
  Drupal.ucCCfee.$feePane = $(context).find('#cc_fee-pane');
  Drupal.ucCCfee.$feeRadios = Drupal.ucCCfee.$feePane.find('.form-radios.uc_cc_fee-radios');
  Drupal.ucCCfee.$paymentRadios = $(context).find('#payment-pane .form-radios');

  // Initial hide fee pane
  Drupal.ucCCfee.$feePane
    .hide()

    // Pseudo requried field. Original form orange star(*) next to form label.
    // This is only visual effect and it's need to be finished on server side
    // with validation function.
    .find('label:contains("Credit card fee")')
      .append('<span class="form-required" title="' + Drupal.t('This field is required.') + '">*</span>')

  // Bind fees radios change with line item action
  Drupal.ucCCfee.fees();

  // Remeber fee we would like to react
  var feePayment = 'credit', feeName;

  if (Drupal.ucCCfee.$paymentRadios.find('input').size() > 0) {
    // For all payment radios (credit, check, paypal, cod, etc..)
    Drupal.ucCCfee.$paymentRadios
      // Initial check. If payment is 'credit' then immediately activate fee pane.
      .find('input:checked')
        .each(function(i, radio) {
          if ($(radio).val() == feePayment) {
            Drupal.ucCCfee.pane('show');
          }
        })
        .end()

      // When 'Credit card' payment has changed
      .change(function() {
        if ($(this).find('input:checked').val() == feePayment) {
          Drupal.ucCCfee.pane('show');
        } else {
          Drupal.ucCCfee.pane('hide');
        }
    });

  // If "Hide the payment method radio buttons if only one method is available."
  // is turn on by uc_checkout_tweks module and the only payment method is
  // 'credit' then show fee pane.
  } else {
    if (Drupal.settings.ucCCfee.creditIsOnlyMethod == 1) {
      Drupal.ucCCfee.pane('show');
    }
  }
};

Drupal.ucCCfee.pane = function(action) {
  if (action == 'show') {
    if ($(Drupal.ucCCfee.$feePane).is(':hidden')) {
      Drupal.ucCCfee.$feePane.show();
    }

    // If fee pane radio is selected then add line item. It's happening
    // when user comming back from order review. Also when you simply
    // reload the checkout page.
    if (feeName = Drupal.ucCCfee.$feeRadios.find(':checked').val()) {
      Drupal.ucCCfee.setLineItem(feeName);
    }

    // Rmember that payment method is credit
    Drupal.ucCCfee.$feePane.find('#edit-panes-cc-fee-payment-method-credit').val(1);

  } else {
    if ($(Drupal.ucCCfee.$feePane).is(':visible')) {
      Drupal.ucCCfee.$feePane.hide();

      // Unselect fee pane
      Drupal.ucCCfee.$feeRadios.each(function(i,v) {
        $(v).find('input').attr('checked', false);
      });

      // ..and unset line item
      remove_line_item('uc_cc_fee');
    }

    // Payment method is NOT credit
    Drupal.ucCCfee.$feePane.find('#edit-panes-cc-fee-payment-method-credit').val(0);
  }
}

// Add behavior on fee click
Drupal.ucCCfee.fees = function() {
  Drupal.ucCCfee.$feeRadios.find(".form-item label input")
    .click(function() {
      Drupal.ucCCfee.setLineItem($(this).val());
  });
};

// Helper function. Prepare line item title
Drupal.ucCCfee.setLineItem = function(feeName) {
  var fee = Drupal.settings.ucCCfee.fees[feeName],
      feeValue, calculatedFee, title;

  // if % value
  if (fee.value.charAt(fee.value.length-1) == '%') {
    // remove last character (%) so we can make calculation
    feeValue = fee.value.slice(0, -1) / 100;
    calculatedFee = parseFloat(Drupal.settings.ucCCfee.subtotal) * parseFloat(feeValue);
    title = fee.line_item;

  // .. else number value
  } else {
    calculatedFee = parseFloat(fee.value);
    title = fee.line_item;
  }

  set_line_item('uc_cc_fee', title, calculatedFee, 10);
}
