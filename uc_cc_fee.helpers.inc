<?php

/**
 * Helper functions to handle uc_cc_fee variables
 */
function _uc_cc_fee_get_var_name($name) {
  return "uc_cc_fee_var_{$name}_fee";
}

function uc_cc_fee_get_all_vars_names() {
  // Give ability to overwrite conf.
  module_invoke_all('uc_cc_fee_conf');

  global $conf;

  return preg_grep("/^uc_cc_fee_var_[a-z0-9_]+_fee$/", array_keys($conf));
}

function _uc_cc_fee_get_operations($fee_id) {
  $edit = l(t('edit'), 'admin/store/settings/cc_fee/edit/'. $fee_id);

  $t_delete = t('delete');
  $delete = (count(uc_cc_fee_cards_data()) > 1) ?  l($t_delete, 'admin/store/settings/cc_fee/delete/'. $fee_id) : $t_delete;

  return "{$edit} | {$delete}";
}

/**
 * Helper method. If credit method is activated and only payment method in
 * checkout.
 */
function _uc_cc_fee_is_credit_method() {
    $methods = array_filter(_payment_method_list(), '_uc_cc_fee_filter_credit');

    // Set (overwrite) array keys from 0 forward
    sort($methods);

    // If method is 'credit' and is the ONLY method
    if (count($methods) == 1 && $methods[0]['id'] == 'credit') {
      return 1;
    } else {
      return 0;
    }
}

/**
 * Helper function. Return only enabled payment methods.
 */
function _uc_cc_fee_filter_credit($method) {
  return $method['checkout'] === 1 ? 1 : 0;
}

/**
 * Prepare all fees data in one array and return it.
 *
 * @return array of fees data
 */
function uc_cc_fee_cards_data($fee_formatted = FALSE) {
  $fees = array();
  foreach(uc_cc_fee_get_all_vars_names() as $fee_full_id) {
    $fee_id = _uc_cc_fee_real_id($fee_full_id);
    $fees[$fee_id] = variable_get($fee_full_id, ''); // set fee data

    if($fee_formatted) {
      $fees[$fee_id]['formatted'] = uc_cc_fee_format_value($fees[$fee_id]['value']);
    }
  }

  // Sort fees by 'weight'. This is the way how to recreate saved order in drag
  // & drop table rows.
  uasort($fees, '_uc_cc_fee_weight_comparation');

  return $fees;
}

/**
 * Helper function.
 *
 * @see uc_cc_fee_cards_data()
 */
function _uc_cc_fee_weight_comparation(array $a, array $b) {
  return $a['weight'] - $b['weight'];
}

/**
 * Helper function get full-real fee ID.
 */
function _uc_cc_fee_real_id($fee_full_id) {
  return preg_replace('/^uc_cc_fee_var_(\w+)_fee$/i', '$1', $fee_full_id);
}

/**
 * Helper function to load fee data. Used in menu /%fee_name.
 *
 * @param $fee_name basic fee name
 * @param $value is set then returning only value of fee object
 *
 * @return
 *   Full object with full variable name in it or only fee value is $value is
 *   is set up and exist, NULL otherwise.
 */
function uc_cc_fee_load($fee_name, $value = NULL) {
  if (!isset($fee_name) || !is_string($fee_name)) {
    return NULL;
  }

  $fee_full_name = _uc_cc_fee_get_var_name($fee_name);
  $fee = variable_get($fee_full_name, NULL);
  if ($fee !== NULL) {
    $fee = (object) $fee;
    $fee->var_name = $fee_full_name;
    if ($value !== NULL) {
      if (isset($fee->$value)) {
        return $fee->$value;
      } else {
        return NULL;
      }
    }
    return $fee;
  } else {
    return NULL;
  }
}

/**
 * Render fee fee title name.
 *
 * @param Fee array
 *
 * @return rendered title name.
 */
function uc_cc_fee_format_option_name($fee_id, &$fee) {
  $data = array(
    'fee_id' => $fee_id,
    'option_name' => $fee['option_name'] ? $fee['option_name'] : CC_FEE_DEFAULT_OPTION_NAME,
  );

  return theme('uc_cc_fee_option', $data);
}

/**
 * Helper function to format price if it's not percentage.
 *
 * @param Fee value
 *
 * @return Formatted fee value
 */
function uc_cc_fee_format_value($value) {
  return _uc_cc_fee_is_percentage($value) ? $value : uc_currency_format($value);
}

/**
 * Helper function. Check if last character of string is '%'.
 *
 * @param $string - string to check
 *
 * @return True if last char is '%', false otherwise.
 */
function _uc_cc_fee_is_percentage($string) {
  return substr($string, -1) == '%' ? TRUE : FALSE;
}

/**
 * Calculate credit card fee to pay.
 *
 * @return Calculated fee against amount.
 */
function uc_cc_fee_amount($fee, $amount) {
  if (_uc_cc_fee_is_percentage($fee)) {
    return _uc_cc_fee_calc_percentage($fee, $amount);
  } else {
    return (float) $fee;
  }
}

/**
 * Calculate percentage fee amount value.
 */
function _uc_cc_fee_calc_percentage($fee_percentage, $amount) {
  return $amount * (rtrim($fee_percentage, '%') / 100);
}

/**
 * Check are we on fees settings page.
 *
 * @return true if we are, false otherwise.
 */
function _uc_cc_fee_settings_page() {
  return implode('/', arg()) == 'admin/store/settings/payment/edit/methods';
}

function _uc_cc_fee_avaiable_tokens() {
  $tokens = uc_cc_fee_token_list();
  foreach($tokens['cc_fee'] as $token_var => $token_info) {
    $uc_cc_fee_tokens[] = "[{$token_var}]";
  }

  return implode(', ', $uc_cc_fee_tokens);
}

function _uc_cc_fee_display_on($fee) {
  $fee = (object) $fee;
  $output = array();
  $front = t('front');
  array_push($output, isset($fee->front_end) ? ($fee->front_end == 1 ? $front : '') : $front);

  $back = t('back');
  array_push($output, isset($fee->back_end) ? ($fee->back_end == 1 ? $back : '') : $back);

  return implode(' & ', array_filter($output)). ' end';
}
