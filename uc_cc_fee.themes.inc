<?php

/**
 * Theme the profile field overview into a drag and drop enabled table.
 */
function theme_uc_cc_fee_settings_table_form($form) {
  // Add javascript
  $path = drupal_get_path('module', 'uc_cc_fee');
  drupal_add_js($path. '/js/uc_cc_fee_backend.js');

  // The variable that will hold our form HTML output
  $table_rows = array();

  // Loop through each "row" in the table array
  foreach (element_children($form['fees']) as $row_key) {
    $fee_row = $form['fees'][$row_key];

    // .. and render all elements
    $this_row = array();
    foreach(element_children($fee_row) as $element_key) {
      array_push($this_row, drupal_render($fee_row[$element_key]));
    }

    // Add the row to the array of rows
    $table_rows[] = array('data' => $this_row, 'class' => 'draggable');
  }

  // Make sure the header count matches the column count
  $headers = $form['fees']['#_fee_headers'];

  // CSS table ID
  $table_id = 'uc-cc-fee-settings-table-form';

  // This function is what brings in the javascript to make our table
  // drag-and-droppable
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'uc-cc-fee-settings-table-form-weight');

  // Theme fees table
  $form['fees'] = array(
    '#type' => 'markup',
    '#value' => theme('table', $headers, $table_rows, array('id' => $table_id)),
    '#weight' => '1',
  );

  //Render the form
  return drupal_render($form);
}

/**
 * Theme on fees on checkout page.
 */
function theme_uc_cc_fee_radios_options($fees) {
  $path = implode('/', arg());
  $radios_fees = $fees;
  foreach($fees as $fee_id => $fee) {
    // Don't render option if on front end and front end off
    if ($path == 'cart/checkout' && isset($fee['front_end']) && $fee['front_end'] == 0) {
      unset($radios_fees[$fee_id]);
      continue;
    }

    // Don't render option if on back end and back end off
    if ($path != 'cart/checkout' && isset($fee['back_end']) && $fee['back_end'] == 0) {
      unset($radios_fees[$fee_id]);
      continue;
    }

    $data['fee_id'] = $fee_id;
    $data['option_name'] = $fee['option_name'] ? $fee['option_name'] : CC_FEE_DEFAULT_OPTION_NAME;
    $radios_fees[$fee_id] = theme('uc_cc_fee_option', $data);
  }

  return $radios_fees;
}

function theme_uc_cc_fee_option($data) {
  $option = token_replace($data['option_name'], 'cc_fee', $data['fee_id']);
  if (_uc_cc_fee_settings_page() && $data['option_name'] == CC_FEE_DEFAULT_OPTION_NAME) {
    $option = theme('uc_cc_fee_default_view', $option);
  }

  return $option;
}

function theme_uc_cc_fee_line_item_name($fee_id, $fee) {
  // Covert to object if it's not an object
  if(!is_object($fee)) {
    $fee = (object) $fee;
  }

  $line_item_name = $fee->line_item_name ? $fee->line_item_name : CC_FEE_DEFAULT_LINE_ITEM_NAME;
  $fee_line_item = token_replace($line_item_name, 'cc_fee', $fee_id);
  if (_uc_cc_fee_settings_page() && $line_item_name == CC_FEE_DEFAULT_LINE_ITEM_NAME) {
    $fee_line_item = theme('uc_cc_fee_default_view', $fee_line_item);
  }

  return $fee_line_item;
}

function theme_uc_cc_fee_label() {
  return '<strong>'. t('Credit card fee'). '</strong>: <span id="uc_cc_fee_label_value">?</span>';
}

function theme_uc_cc_fee_default_view($view) {
  return $view .= "&nbsp;<sup style='color: #B8B8B8'>(". t('default') .")</sup>";
}